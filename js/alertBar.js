let removeAlert = () => {
  let body = document.getElementsByTagName("BODY")[0];
  let alertBar = document.getElementById("alertBar");
  body.removeChild(alertBar);
};

let showAlert = content => {
  let body = document.getElementsByTagName("BODY")[0];
  //Create alert bar
  let alertBar = document.createElement("div");
  alertBar.className = "alertBar";
  alertBar.id = "alertBar";

  //Alertbar content
  let alertBarContent = document.createElement("div");
  alertBarContent.className = "alertBarContent";

  let contentParagraph = document.createElement("p");
  contentParagraph.innerHTML = content;

  let closeAlertButton = document.createElement("div");
  closeAlertButton.innerHTML = "x";
  closeAlertButton.className = "closeAlertButton";
  closeAlertButton.addEventListener("click", removeAlert);

  alertBarContent.insertAdjacentElement("afterbegin", contentParagraph);
  alertBarContent.insertAdjacentElement("beforeend", closeAlertButton);
  alertBar.insertAdjacentElement("afterbegin", alertBarContent);
  body.insertAdjacentElement("afterbegin", alertBar);
};
