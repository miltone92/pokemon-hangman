/**
 * Global varaibles
 */

String.prototype.replaceAt = function(index, replacement) {
  return (
    this.substr(0, index) +
    replacement +
    this.substr(index + replacement.length)
  );
};

let lives = 7;
let fails = 4;
let wordToGuess;
let won = false;
let myGuess = "";

let words = [
  "apple",
  "book",
  "corn",
  "dice",
  "extreme",
  "fake",
  "gold",
  "heavy",
  "internet"
];

/**
 * Logic
 */

let getRandomNumber = topLimit => {
  return Math.ceil(Math.random() * topLimit - 1);
};

let generateWordToGuess = () => {
  let dataSize = pokemonList.length;

  if (dataSize > 0) {
    let randomIndex = getRandomNumber(dataSize);
    let randomWordToGuess = pokemonList[randomIndex];
    return randomWordToGuess;
  }

  return null;
};

readGuessValue = () => {
  //Define input and button
  let guessInput = document.getElementById("guessInput");
  let guess = guessInput.value;
  return guess;
};

let guess = () => {
  let guessValue = readGuessValue();
  let success = updateProgress(guessValue);
  if (!success) {
    lives--;
    fails++;
    let hangmanImage = document.getElementById("hangmanImage");
    hangmanImage.src = `./img/${fails}.svg`;
  }
  updateLivesCounter();
  checkWin();
  checkLoss();
};

generateProgress = () => {
  let progress = document.getElementById("progress");
  let progressHTML = "";

  for (let i = 0; i < wordToGuess.length; i++) {
    progressHTML += "_ ";
  }
  progress.innerHTML = progressHTML;
};

updateProgress = guessValue => {
  let success = false;
  if (guessValue.length == 1) {
    for (let i = 0; i < wordToGuess.length; i++) {
      let letter = wordToGuess.charAt(i);
      if (letter == guessValue) {
        success = true;
        let progress = document.getElementById("progress");
        let progressHTML = progress.innerHTML;

        //let character = progressHTML.charAt(i * 2);
        let progressIndex = i * 2;
        progressHTML = progressHTML.replaceAt(progressIndex, letter);
        progress.innerHTML = progressHTML;
        myGuess = progressHTML.replace(/ /g, "");
        myGuess = myGuess.replace(/ /g, "_");
        console.log(myGuess);
      }
    }
  }

  return success;
};

let showPokemon = json => {
  console.log("on show");
  console.log(json);
  window.location = `/pokedex.html?name=${json.name}`;
};

let checkWin = () => {
  if (wordToGuess == myGuess) {
    showAlert("You win");
    let pokemon = getPokemonData(myGuess, showPokemon);

    //alert("You win");
  }
};

let checkLoss = () => {
  if (lives <= 0) {
    // alert("You loose :(");
    showAlert("You loose :(");
  }
};

let updateLivesCounter = () => {
  let hpCounter = document.getElementById("hpCounter");
  hpCounter.innerHTML = lives;
};

let playGame = () => {
  //Generate word
  wordToGuess = generateWordToGuess();

  console.log(wordToGuess);

  //Create progress bar
  generateProgress();

  //add function to button
  let guessButton = document.getElementById("guessButton");
  guessButton.addEventListener("click", guess);
};

/**
 * Pokemon api
 */
const MAX_PKM_TO_LOAD = "151";
const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
let pokemonList = [];

let getPokemon = async () => {
  let GET_PKM_LIST_ENDPOINT = `${BASE_URL}?limit=${MAX_PKM_TO_LOAD}`;

  fetch(GET_PKM_LIST_ENDPOINT)
    .then(response => {
      return response.json();
    })
    .then(json => {
      console.log(json);
      for (const pkm of json.results) {
        pokemonList.push(pkm.name);
      }
      playGame();
    });
};

let getPokemonData = (name, callback) => {
  let GET_PKM_LIST_ENDPOINT = `${BASE_URL}${name}`;
  fetch(GET_PKM_LIST_ENDPOINT)
    .then(response => {
      return response.json();
    })
    .then(json => {
      console.log(json);
      callback(json);
      return json;
    });
};

getPokemon();
