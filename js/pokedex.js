let pokedexSection = document.getElementById("pokedexSection");

let url_string = window.location.href;
let url = new URL(url_string);
let name = url.searchParams.get("name");
const MAX_PKM_TO_LOAD = "151";
const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";

let getPokemonData = (name, callback) => {
  let GET_PKM_LIST_ENDPOINT = `${BASE_URL}${name}`;
  fetch(GET_PKM_LIST_ENDPOINT)
    .then(response => {
      return response.json();
    })
    .then(json => {
      callback(json);
      return json;
    });
};

let createPokedex = pokemon => {
  console.log(pokemon);
  let sprite = document.createElement("img");
  sprite.src = pokemon.sprites.front_default;
  pokedexSection.appendChild(sprite);

  let shinySprite = document.createElement("img");
  shinySprite.src = pokemon.sprites.front_shiny;
  pokedexSection.appendChild(shinySprite);

  let name = document.createElement("p");
  name.innerHTML = "Name: ";
  name.innerHTML += pokemon.name;
  pokedexSection.appendChild(name);

  let id = document.createElement("p");
  id.innerHTML = "ID: ";
  id.innerHTML += pokemon.id;
  pokedexSection.appendChild(id);

  let height = document.createElement("p");
  height.innerHTML = "Height: ";
  height.innerHTML += pokemon.height;
  pokedexSection.appendChild(height);

  let weight = document.createElement("p");
  weight.innerHTML = "Weight: ";
  weight.innerHTML += pokemon.weight;
  pokedexSection.appendChild(weight);
};

getPokemonData(name, createPokedex);
