let sidebar = document.getElementById("sidebar");
let closeButton = document.getElementById("close-sidebar-button");
let openButton = document.getElementById("open-sidebar-button");

let closeSideMenu = () => {
  sidebar.style.marginRight = "-240px";
  sidebar.style.transition = "0.8s";
  openButton.style.display = "flex";
};

let openSideMenu = () => {
  sidebar.style.marginRight = "0px";
  sidebar.style.transition = "0.8s";
  sidebar.style.display = "block";

  openButton.style.display = "none";
};

closeButton.addEventListener("click", closeSideMenu);
openButton.addEventListener("click", openSideMenu);
